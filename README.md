# Talleres POO
# 1.Tangram 

### Propósito
Repasar los conceptos fundamentales de la programacion estructurada en la implementación del tangram.

### Objetivos
1.  Dibujo de las piezas
2.  Selección y manipulación de las piezas (ratón y teclado)
3.  Verificación de resultados
4.  Creación de niveles (opcional)